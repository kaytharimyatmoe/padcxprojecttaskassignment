package com.example.ktmmoe.taskapp.mvp.presenters

import com.example.ktmmoe.taskapp.mvp.views.BaseView

/**
 * Created by ktmmoe on 23, July, 2020
 **/
interface BasePresenter<T : BaseView> {
    fun initPresenter(view: T)
}