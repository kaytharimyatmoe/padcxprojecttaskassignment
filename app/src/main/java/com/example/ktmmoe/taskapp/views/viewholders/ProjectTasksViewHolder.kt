package com.example.ktmmoe.taskapp.views.viewholders

import android.view.View
import com.example.ktmmoe.taskapp.delegates.ProjectTasksItemDelegate
import kotlinx.android.synthetic.main.item_project_tasks.view.*

/**
 * Created by ktmmoe on 24, July, 2020
 **/
class ProjectTasksViewHolder(itemView: View, delegate: ProjectTasksItemDelegate): BaseViewHolder<String>(itemView) {
    init {
        itemView.roundedCornerImageView.setOnClickListener { delegate.onProjectTasksProfileClicked() }
    }

    override fun bindData(data: String) {}
}