package com.example.ktmmoe.taskapp.views.components

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.withStyledAttributes
import com.example.ktmmoe.taskapp.R

/**
 * Created by ktmmoe on 23, July, 2020
 **/
class RoundedCornerImageView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {

    private var mCornerRadius = 0f
    private var mStrokeWidth = 4f
    private var mStrokeColor = Color.GRAY
    private val path = Path()

    private val mBorderPaint = Paint()

    init {
        context.withStyledAttributes(attrs, R.styleable.RoundedCornerImageView) {
            mCornerRadius = getDimension(R.styleable.RoundedCornerImageView_cornerRadius, 0f)
            mStrokeWidth = getDimension(R.styleable.RoundedCornerImageView_strokeWidth, 0f)
            mStrokeColor = getColor(R.styleable.RoundedCornerImageView_strokeColor, Color.GRAY)
        }
        mBorderPaint.apply {
            mBorderPaint.style = Paint.Style.STROKE
            color = mStrokeColor
            strokeWidth = mStrokeWidth
        }
    }

    override fun onDraw(canvas: Canvas?) {
        val rectangle = RectF(0f, 0f, width.toFloat() , height.toFloat())

        path.addRoundRect(rectangle, mCornerRadius, mCornerRadius, Path.Direction.CCW)

//        canvas?.clipPath(path)
//        canvas?.drawOval(1f, 1f, width.toFloat() + 1f, height.toFloat() + 1f, mBorderPaint)
        canvas?.drawPath(path, mBorderPaint)
        super.onDraw(canvas)
    }

}