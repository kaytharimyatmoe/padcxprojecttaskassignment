package com.example.ktmmoe.taskapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ktmmoe.taskapp.R
import com.example.ktmmoe.taskapp.adapters.ProjectTasksRecyclerAdapter
import com.example.ktmmoe.taskapp.mvp.presenters.ProjectTasksPresenter
import com.example.ktmmoe.taskapp.mvp.presenters.impls.ProjectTasksPresenterImpl
import com.example.ktmmoe.taskapp.mvp.views.ProjectTasksView
import kotlinx.android.synthetic.main.fragment_project_tasks.*

class ProjectTasksFragment : Fragment(), ProjectTasksView {
    private lateinit var mAdapter : ProjectTasksRecyclerAdapter
    private lateinit var mPresenter: ProjectTasksPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_project_tasks, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupPresenter()
        setupRecyclerView()
        mPresenter.onUiReady(this)
    }

    private fun setupPresenter() {
        mPresenter = ViewModelProviders.of(this).get(ProjectTasksPresenterImpl::class.java)
        mPresenter.initPresenter(this)
    }

    private fun setupRecyclerView() {
        mAdapter = ProjectTasksRecyclerAdapter(mPresenter)
        val mLayoutManager = LinearLayoutManager(requireContext())
        this.recyclerview.apply {
            layoutManager = mLayoutManager
            adapter = mAdapter
        }
    }

    override fun displayProjectTaskList() {
        mAdapter.setNewData(mutableListOf("1", "2", "", "", ""))
    }

    override fun navigateToProfile() {
        val bottomSheetFragment = ProfileBottomSheetFragment()
        bottomSheetFragment.show(requireFragmentManager(), bottomSheetFragment.tag)
    }

    companion object {
        fun newInstance() : ProjectTasksFragment {
            return ProjectTasksFragment()
        }
    }
}