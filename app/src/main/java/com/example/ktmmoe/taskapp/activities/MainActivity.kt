package com.example.ktmmoe.taskapp.activities

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ktmmoe.taskapp.R
import com.example.ktmmoe.taskapp.adapters.ProfileRecyclerAdapter
import com.example.ktmmoe.taskapp.adapters.ProjectTasksRecyclerAdapter
import com.example.ktmmoe.taskapp.fragments.ProfileBottomSheetFragment
import com.example.ktmmoe.taskapp.mvp.presenters.MainPresenter
import com.example.ktmmoe.taskapp.mvp.presenters.impls.MainPresenterImpl
import com.example.ktmmoe.taskapp.mvp.views.MainView
import com.example.ktmmoe.taskapp.views.itemdecoration.HorizontalOverlapDecoration
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), MainView {
    private lateinit var mAdapter: ProfileRecyclerAdapter
    private lateinit var mTasksAdapter: ProjectTasksRecyclerAdapter

    private lateinit var mPresenter : MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupPresenter()
        setupProfileRecyclerView()
        setupTasksRecyclerView()
        mPresenter.onUiReady(this)
    }

    private fun setupPresenter() {
        mPresenter = ViewModelProviders.of(this).get(MainPresenterImpl::class.java)
        mPresenter.initPresenter(this)
    }

    private fun setupProfileRecyclerView() {
        mAdapter = ProfileRecyclerAdapter(mPresenter)
        val mLayoutManager = LinearLayoutManager(this)
            .apply {
                orientation = LinearLayoutManager.HORIZONTAL
                stackFromEnd = true
            }

        recyclerview.apply {
            layoutManager = mLayoutManager
            addItemDecoration(HorizontalOverlapDecoration())
            adapter = mAdapter
        }
    }

    private fun setupTasksRecyclerView() {
        mTasksAdapter = ProjectTasksRecyclerAdapter(mPresenter)
        val mLayoutManager = LinearLayoutManager(this)
        tasksRecyclerview.layoutManager = mLayoutManager
        tasksRecyclerview.adapter = mTasksAdapter
        mTasksAdapter.setNewData(mutableListOf("", "", "", ""))
    }

    override fun displayProfileList() {
        mAdapter.setNewData(mutableListOf("a", "b", "c", "d", "e"))
    }

    override fun navigateToProfileDetail() {
        val bottomSheetFragment = ProfileBottomSheetFragment()
        bottomSheetFragment.show(supportFragmentManager, bottomSheetFragment.tag)
    }

    override fun navigateToCreateTaskActivity() {
        startActivity(CreateTaskActivity.newIntent(this))
    }

}