package com.example.ktmmoe.taskapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.ktmmoe.taskapp.R
import com.example.ktmmoe.taskapp.delegates.ProjectTasksItemDelegate
import com.example.ktmmoe.taskapp.views.viewholders.ProjectTasksViewHolder

/**
 * Created by ktmmoe on 24, July, 2020
 **/
class ProjectTasksRecyclerAdapter(delegate: ProjectTasksItemDelegate): BaseRecyclerAdapter<ProjectTasksViewHolder, String>() {
    private val mDelegate = delegate

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProjectTasksViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_project_tasks, parent, false)
        return ProjectTasksViewHolder(view, mDelegate)
    }
}