package com.example.ktmmoe.taskapp.mvp.presenters

import androidx.lifecycle.LifecycleOwner
import com.example.ktmmoe.taskapp.delegates.ProfileItemDelegate
import com.example.ktmmoe.taskapp.mvp.views.CreateTaskView
import com.example.ktmmoe.taskapp.views.viewpods.CategoryViewPod

/**
 * Created by ktmmoe on 25, July, 2020
 **/
interface CreateTaskPresenter: BasePresenter<CreateTaskView>, ProfileItemDelegate {
    fun onUiReady(lifecycleOwner: LifecycleOwner)
}