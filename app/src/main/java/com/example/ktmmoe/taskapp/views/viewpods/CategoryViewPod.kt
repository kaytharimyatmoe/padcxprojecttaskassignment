package com.example.ktmmoe.taskapp.views.viewpods

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.cardview.widget.CardView
import com.example.ktmmoe.taskapp.delegates.ProfileReactionDelegate
import com.google.android.material.card.MaterialCardView
import kotlinx.android.synthetic.main.view_pod_category.view.*

/**
 * Created by ktmmoe on 25, July, 2020
 **/
class CategoryViewPod @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : CardView(context, attrs, defStyleAttr) {

    override fun onFinishInflate() {
        super.onFinishInflate()
        setUpListener()
    }

    fun setCategoryName(category: String) {
        tvCategory.text = category
    }

    private fun clickedStateToggle() {
        if (ivCheck.visibility == View.GONE)
            clickedState()
        else unClickedState()
    }

    private fun clickedState() {
        ivCheck.visibility= View.VISIBLE
        tvCategory.setTextColor(Color.WHITE)
        cvCategory.setCardBackgroundColor(Color.DKGRAY)
    }

    private fun unClickedState() {
        ivCheck.visibility = View.GONE
        tvCategory.setTextColor(Color.DKGRAY)
        cvCategory.setCardBackgroundColor(Color.WHITE)
    }

    private fun setUpListener() {
        cvCategory.setOnClickListener { clickedStateToggle() }
    }
}