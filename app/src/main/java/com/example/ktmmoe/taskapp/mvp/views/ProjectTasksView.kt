package com.example.ktmmoe.taskapp.mvp.views

/**
 * Created by ktmmoe on 25, July, 2020
 **/
interface ProjectTasksView: BaseView {
    fun displayProjectTaskList()
    fun navigateToProfile()
}