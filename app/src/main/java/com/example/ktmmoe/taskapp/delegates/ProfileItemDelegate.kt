package com.example.ktmmoe.taskapp.delegates

/**
 * Created by ktmmoe on 23, July, 2020
 **/
interface ProfileItemDelegate {
    fun onProfileItemClick()

    fun onCreateTaskClick()
}