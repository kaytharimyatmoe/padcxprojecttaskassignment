package com.example.ktmmoe.taskapp.activities

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import java.util.*
import javax.xml.datatype.DatatypeConstants.MONTHS

/**
 * Created by ktmmoe on 23, July, 2020
 **/
abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun showSnackBar(message : String){
        Snackbar
            .make(window.decorView, message, Snackbar.LENGTH_LONG).show()
    }
}