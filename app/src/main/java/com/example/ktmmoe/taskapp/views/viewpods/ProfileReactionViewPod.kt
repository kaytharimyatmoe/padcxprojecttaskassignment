package com.example.ktmmoe.taskapp.views.viewpods

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.example.ktmmoe.taskapp.delegates.ProfileReactionDelegate
import kotlinx.android.synthetic.main.view_pod_profile_reaction.view.*

/**
 * Created by ktmmoe on 25, July, 2020
 **/
class ProfileReactionViewPod @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private var mDelegate: ProfileReactionDelegate? = null

    override fun onFinishInflate() {
        super.onFinishInflate()
        setUpListener()
    }

    fun setDelegate(delegate: ProfileReactionDelegate) {
        mDelegate = delegate
    }

    private fun setUpListener() {
        ivShare.setOnClickListener {
            mDelegate?.onShareClicked()
        }

        btnMessage.setOnClickListener {
            mDelegate?.onMessageClicked()
        }

        ivAssignTask.setOnClickListener {
            mDelegate?.onAssignTaskClicked()
        }
    }
}