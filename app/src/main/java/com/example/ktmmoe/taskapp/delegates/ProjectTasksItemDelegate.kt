package com.example.ktmmoe.taskapp.delegates

/**
 * Created by ktmmoe on 24, July, 2020
 **/
interface ProjectTasksItemDelegate {
    fun onProjectTasksProfileClicked()
}