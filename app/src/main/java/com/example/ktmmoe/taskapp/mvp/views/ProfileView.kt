package com.example.ktmmoe.taskapp.mvp.views

/**
 * Created by ktmmoe on 24, July, 2020
 **/
interface ProfileView: BaseView {
    fun shareProfile()
    fun messageToProfile()
    fun assignTaskToProfile()
}