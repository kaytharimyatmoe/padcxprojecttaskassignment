package com.example.ktmmoe.taskapp.mvp.views

/**
 * Created by ktmmoe on 23, July, 2020
 **/
interface MainView: BaseView {
    fun displayProfileList()
    fun navigateToProfileDetail()
    fun navigateToCreateTaskActivity()
}