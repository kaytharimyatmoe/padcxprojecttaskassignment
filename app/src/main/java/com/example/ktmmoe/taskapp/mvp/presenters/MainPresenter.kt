package com.example.ktmmoe.taskapp.mvp.presenters

import androidx.lifecycle.LifecycleOwner
import com.example.ktmmoe.taskapp.delegates.ProfileItemDelegate
import com.example.ktmmoe.taskapp.delegates.ProjectTasksItemDelegate
import com.example.ktmmoe.taskapp.mvp.views.MainView

/**
 * Created by ktmmoe on 23, July, 2020
 **/
interface MainPresenter: BasePresenter<MainView>, ProfileItemDelegate, ProjectTasksItemDelegate {
    fun onUiReady (lifecycleOwner: LifecycleOwner)
}