package com.example.ktmmoe.taskapp.mvp.presenters.impls

import androidx.lifecycle.LifecycleOwner
import com.example.ktmmoe.taskapp.mvp.presenters.AbstractBasePresenter
import com.example.ktmmoe.taskapp.mvp.presenters.ProfilePresenter
import com.example.ktmmoe.taskapp.mvp.views.ProfileView

/**
 * Created by ktmmoe on 24, July, 2020
 **/
class ProfilePresenterImpl: ProfilePresenter, AbstractBasePresenter<ProfileView>() {
    override fun onUiReady(lifecycleOwner: LifecycleOwner) {
    }

    override fun onShareClicked() {
        mView?.shareProfile()
    }

    override fun onMessageClicked() {
        mView?.messageToProfile()
    }

    override fun onAssignTaskClicked() {
        mView?.assignTaskToProfile()
    }
}