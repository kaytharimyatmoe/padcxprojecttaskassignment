package com.example.ktmmoe.taskapp.mvp.presenters

import androidx.lifecycle.LifecycleOwner
import com.example.ktmmoe.taskapp.delegates.ProfileReactionDelegate
import com.example.ktmmoe.taskapp.delegates.ProjectTasksItemDelegate
import com.example.ktmmoe.taskapp.mvp.views.ProfileView

/**
 * Created by ktmmoe on 24, July, 2020
 **/
interface ProfilePresenter: BasePresenter<ProfileView>, ProfileReactionDelegate {
    fun onUiReady (lifecycleOwner: LifecycleOwner)
}