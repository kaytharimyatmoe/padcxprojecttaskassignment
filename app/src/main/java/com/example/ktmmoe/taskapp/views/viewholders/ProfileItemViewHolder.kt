package com.example.ktmmoe.taskapp.views.viewholders

import android.view.View
import com.example.ktmmoe.taskapp.delegates.ProfileItemDelegate

/**
 * Created by ktmmoe on 23, July, 2020
 **/
abstract class AbstractProfileItemViewHolder(itemView: View) : BaseViewHolder<String>(itemView) {}

class ProfileItemViewHolder(itemView: View, delegate: ProfileItemDelegate): AbstractProfileItemViewHolder(itemView) {

    init {
        itemView.setOnClickListener {
            delegate.onProfileItemClick()
        }
    }

    override fun bindData(data: String) {}
}

class CreateTaskViewHolder (itemView: View, delegate: ProfileItemDelegate): AbstractProfileItemViewHolder(itemView) {

    init {
        itemView.setOnClickListener {
            delegate.onCreateTaskClick()
        }
    }

    override fun bindData(data: String) {}

}