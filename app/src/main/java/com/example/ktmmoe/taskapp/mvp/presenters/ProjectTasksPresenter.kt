package com.example.ktmmoe.taskapp.mvp.presenters

import androidx.lifecycle.LifecycleOwner
import com.example.ktmmoe.taskapp.delegates.ProjectTasksItemDelegate
import com.example.ktmmoe.taskapp.mvp.views.ProjectTasksView

/**
 * Created by ktmmoe on 25, July, 2020
 **/
interface ProjectTasksPresenter: ProjectTasksItemDelegate, BasePresenter<ProjectTasksView> {
    fun onUiReady(lifecycleOwner: LifecycleOwner)
}