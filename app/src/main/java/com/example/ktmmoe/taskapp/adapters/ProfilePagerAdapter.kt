package com.example.ktmmoe.taskapp.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.ktmmoe.taskapp.fragments.ProjectTasksFragment

/**
 * Created by ktmmoe on 24, July, 2020
 **/
class ProfilePagerAdapter(fm: FragmentManager, behavior: Int) :
    FragmentStatePagerAdapter(fm, behavior) {
    private val count = behavior
    override fun getItem(position: Int): Fragment {
        return ProjectTasksFragment.newInstance()
    }

    override fun getCount(): Int {
        return count
    }

}