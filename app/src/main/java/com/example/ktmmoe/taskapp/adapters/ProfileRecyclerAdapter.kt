package com.example.ktmmoe.taskapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.ktmmoe.taskapp.R
import com.example.ktmmoe.taskapp.delegates.ProfileItemDelegate
import com.example.ktmmoe.taskapp.views.viewholders.AbstractProfileItemViewHolder
import com.example.ktmmoe.taskapp.views.viewholders.CreateTaskViewHolder
import com.example.ktmmoe.taskapp.views.viewholders.ProfileItemViewHolder

/**
 * Created by ktmmoe on 23, July, 2020
 **/
class ProfileRecyclerAdapter(delegate: ProfileItemDelegate): BaseRecyclerAdapter<AbstractProfileItemViewHolder, String>() {

    private val mDelegate = delegate

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            mData.size - 1 -> ProfileRecyclerType.last
            else -> ProfileRecyclerType.normal
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractProfileItemViewHolder {

        return when(viewType) {
            ProfileRecyclerType.normal -> ProfileItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_profile_list, parent, false), mDelegate)
            else -> CreateTaskViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_create_task, parent, false), mDelegate)
        }
    }
}

sealed class ProfileRecyclerType {
    companion object {
        const val normal : Int = 0
        const val last: Int = 1
    }
}