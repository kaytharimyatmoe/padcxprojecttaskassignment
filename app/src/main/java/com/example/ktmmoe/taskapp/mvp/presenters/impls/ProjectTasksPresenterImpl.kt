package com.example.ktmmoe.taskapp.mvp.presenters.impls

import androidx.lifecycle.LifecycleOwner
import com.example.ktmmoe.taskapp.mvp.presenters.AbstractBasePresenter
import com.example.ktmmoe.taskapp.mvp.presenters.ProjectTasksPresenter
import com.example.ktmmoe.taskapp.mvp.views.ProjectTasksView

/**
 * Created by ktmmoe on 25, July, 2020
 **/
class ProjectTasksPresenterImpl: ProjectTasksPresenter, AbstractBasePresenter<ProjectTasksView>() {
    override fun onUiReady(lifecycleOwner: LifecycleOwner) {
        mView?.displayProjectTaskList()
    }

    override fun onProjectTasksProfileClicked() {
        mView?.navigateToProfile()
    }
}