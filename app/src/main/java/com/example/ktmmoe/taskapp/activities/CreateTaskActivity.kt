package com.example.ktmmoe.taskapp.activities

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ktmmoe.taskapp.R
import com.example.ktmmoe.taskapp.adapters.ProfileRecyclerAdapter
import com.example.ktmmoe.taskapp.fragments.ProfileBottomSheetFragment
import com.example.ktmmoe.taskapp.mvp.presenters.CreateTaskPresenter
import com.example.ktmmoe.taskapp.mvp.presenters.impls.CreateTaskPresenterImpl
import com.example.ktmmoe.taskapp.mvp.views.CreateTaskView
import com.example.ktmmoe.taskapp.views.itemdecoration.HorizontalOverlapDecoration
import com.example.ktmmoe.taskapp.views.viewpods.CategoryViewPod
import kotlinx.android.synthetic.main.activity_create_task.*
import kotlinx.android.synthetic.main.bottom_sheet_create_task.*
import java.util.*

class CreateTaskActivity : BaseActivity(), CreateTaskView {
    private lateinit var mAdapter : ProfileRecyclerAdapter

    private lateinit var mPresenter: CreateTaskPresenter

    private lateinit var mViewPodCategoryDesign: CategoryViewPod
    private lateinit var mViewPodCategoryFrontEnd: CategoryViewPod
    private lateinit var mViewPodCategoryBackEnd: CategoryViewPod

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_task)

        setupPresenter()
        setupViewPods()
        setupListeners()
        setupAssigneeRecyclerview()
        mPresenter.onUiReady(this)
    }

    private fun setupListeners() {
        etFromDate.setOnClickListener { showDatePicker{
            etFromDate.setText(it)
        } }

        etToDate.setOnClickListener {
            showDatePicker{date ->
                etToDate.setText(date)
            }
        }

        btnCreateTask.setOnClickListener { mPresenter.onCreateTaskClick() }
    }

    private fun setupPresenter() {
        mPresenter = ViewModelProviders.of(this).get(CreateTaskPresenterImpl::class.java)
        mPresenter.initPresenter(this)
    }

    private fun setupViewPods() {
        mViewPodCategoryDesign = viewPodCategoryDesign as CategoryViewPod
        mViewPodCategoryFrontEnd = viewPodCategoryFrontEnd as CategoryViewPod
        mViewPodCategoryBackEnd = viewPodCategoryBackEnd as CategoryViewPod

        mViewPodCategoryDesign.setCategoryName("Design")
        mViewPodCategoryFrontEnd.setCategoryName("Front End")
        mViewPodCategoryBackEnd.setCategoryName("Back End")
    }

    private fun setupAssigneeRecyclerview() {
        mAdapter = ProfileRecyclerAdapter(mPresenter)
        recyclerview.apply {
            layoutManager = LinearLayoutManager(this@CreateTaskActivity)
                .apply {
                    orientation = LinearLayoutManager.HORIZONTAL
                    addItemDecoration(HorizontalOverlapDecoration())
                }
            adapter = mAdapter
        }
    }

    override fun displayAssigneeList() {
        mAdapter.setNewData(mutableListOf("", "", ""))
    }

    override fun navigateToProfileDetail() {
        val bottomSheetFragment = ProfileBottomSheetFragment()
        bottomSheetFragment.show(supportFragmentManager, bottomSheetFragment.tag)
    }

    override fun createNewTask() {
        //TODO should properly create a new Task
        finish()
    }

    private fun showDatePicker(callback: (String)->Unit) {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)


        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
           callback.invoke("$monthOfYear, $dayOfMonth, $year")
        }, year, month, day)

        dpd.show()
    }

    companion object {
        fun newIntent(context: Context) : Intent{
            return Intent (context, CreateTaskActivity::class.java)
        }
    }
}