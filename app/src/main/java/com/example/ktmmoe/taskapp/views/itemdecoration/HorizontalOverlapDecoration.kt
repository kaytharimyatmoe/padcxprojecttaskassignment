package com.example.ktmmoe.taskapp.views.itemdecoration

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by ktmmoe on 23, July, 2020
 **/
class HorizontalOverlapDecoration: RecyclerView.ItemDecoration() {
    private val horizontalOverlap = -20
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.set(0, 0, horizontalOverlap, 0)
    }
}