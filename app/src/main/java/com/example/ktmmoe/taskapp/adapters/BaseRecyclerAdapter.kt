package com.example.ktmmoe.taskapp.adapters

import androidx.recyclerview.widget.RecyclerView
import com.example.ktmmoe.taskapp.views.viewholders.BaseViewHolder

/**
 * Created by ktmmoe on 23, July, 2020
 **/
abstract class BaseRecyclerAdapter<T : BaseViewHolder<W>,W>
    : RecyclerView.Adapter<T>() {

    var mData : MutableList<W> = arrayListOf()

    override fun getItemCount(): Int {
        return mData.count()
    }

    override fun onBindViewHolder(holder: T, position: Int) {
        holder.bindData(mData[position])
    }

    fun setNewData(data : MutableList<W>){
        mData = data
        notifyDataSetChanged()
    }

    fun appendNewData(data : List<W>){
        mData.addAll(data)
        notifyDataSetChanged()
    }
}