package com.example.ktmmoe.taskapp.mvp.presenters.impls

import androidx.lifecycle.LifecycleOwner
import com.example.ktmmoe.taskapp.mvp.presenters.AbstractBasePresenter
import com.example.ktmmoe.taskapp.mvp.presenters.CreateTaskPresenter
import com.example.ktmmoe.taskapp.mvp.views.CreateTaskView

/**
 * Created by ktmmoe on 25, July, 2020
 **/
class CreateTaskPresenterImpl: CreateTaskPresenter, AbstractBasePresenter<CreateTaskView>() {
    override fun onUiReady(lifecycleOwner: LifecycleOwner) {
        mView?.displayAssigneeList()
    }

    override fun onProfileItemClick() {
        mView?.navigateToProfileDetail()
    }

    override fun onCreateTaskClick() {
        mView?.createNewTask()
    }
}