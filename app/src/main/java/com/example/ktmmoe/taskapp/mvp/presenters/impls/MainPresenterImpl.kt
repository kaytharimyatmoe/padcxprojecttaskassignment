package com.example.ktmmoe.taskapp.mvp.presenters.impls

import androidx.lifecycle.LifecycleOwner
import com.example.ktmmoe.taskapp.mvp.presenters.AbstractBasePresenter
import com.example.ktmmoe.taskapp.mvp.presenters.MainPresenter
import com.example.ktmmoe.taskapp.mvp.views.MainView

/**
 * Created by ktmmoe on 23, July, 2020
 **/
class MainPresenterImpl: MainPresenter, AbstractBasePresenter<MainView>() {
    override fun onUiReady(lifecycleOwner: LifecycleOwner) {
        mView?.displayProfileList()
    }

    override fun onProfileItemClick() {
        mView?.navigateToProfileDetail()
    }

    override fun onCreateTaskClick() {
        mView?.navigateToCreateTaskActivity()
    }

    override fun onProjectTasksProfileClicked() {
        mView?.navigateToProfileDetail()
    }
}