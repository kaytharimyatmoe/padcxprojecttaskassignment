package com.example.ktmmoe.taskapp.mvp.views

/**
 * Created by ktmmoe on 25, July, 2020
 **/
interface CreateTaskView: BaseView {
    fun displayAssigneeList()
    fun navigateToProfileDetail()
    fun createNewTask()
}