package com.example.ktmmoe.taskapp.fragments

import android.content.res.ColorStateList
import android.graphics.Color.*
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.example.ktmmoe.taskapp.R
import com.example.ktmmoe.taskapp.adapters.ProfilePagerAdapter
import com.example.ktmmoe.taskapp.mvp.presenters.ProfilePresenter
import com.example.ktmmoe.taskapp.mvp.presenters.impls.ProfilePresenterImpl
import com.example.ktmmoe.taskapp.mvp.views.ProfileView
import com.example.ktmmoe.taskapp.views.viewpods.ProfileReactionViewPod
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.layout_profile_detail.*

/**
 * Created by ktmmoe on 24, July, 2020
 **/
class ProfileBottomSheetFragment : BottomSheetDialogFragment(), ProfileView {
    private var fragmentView: View? = null

    private lateinit var mPresenter: ProfilePresenter

    private lateinit var mProfileReactionViewPod: ProfileReactionViewPod

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (view!!.parent as View).backgroundTintList = ColorStateList.valueOf(TRANSPARENT)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentView = inflater.inflate(R.layout.layout_profile_detail, container, false)
        return fragmentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupPresenter()
        setupViewPod()

        setupPagerWithTab()
        mPresenter.onUiReady(this)
    }

    private fun setupViewPod() {
        mProfileReactionViewPod = profileReactionViewPod as ProfileReactionViewPod
        mProfileReactionViewPod.setDelegate(mPresenter)
    }

    private fun setupPresenter() {
        mPresenter = ViewModelProviders.of(this).get(ProfilePresenterImpl::class.java)
        mPresenter.initPresenter(this)
    }

    private fun setupPagerWithTab() {
        for (i in tabTitles.indices) {
            tabLayout.addTab(tabLayout.newTab().setText(tabTitles[i]))
        }
        val viewPagerAdapter = ProfilePagerAdapter(childFragmentManager, tabLayout.tabCount)
        viewPager.adapter = viewPagerAdapter
        viewPager.currentItem = 0

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                viewPager.currentItem = tab!!.position
            }

        })
    }

    override fun shareProfile() {
        Toast.makeText(requireContext(), "Profile Share Clicked.", Toast.LENGTH_SHORT).show()
    }

    override fun messageToProfile() {
        Toast.makeText(requireContext(), "Message Btn Clicked.", Toast.LENGTH_SHORT).show()
    }

    override fun assignTaskToProfile() {
        Toast.makeText(requireContext(), "Assign Task Clicked.", Toast.LENGTH_SHORT).show()
    }

    private val tabTitles = listOf("Project Tasks", "Contacts", "About You")
}