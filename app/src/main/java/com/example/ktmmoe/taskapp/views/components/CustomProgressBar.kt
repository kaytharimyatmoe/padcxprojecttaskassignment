package com.example.ktmmoe.taskapp.views.components

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.core.content.withStyledAttributes
import com.example.ktmmoe.taskapp.R
import kotlin.math.min


/**
 * Created by ktmmoe on 24, July, 2020
 **/
class CustomProgressBar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {
    private var max = 100
    private var progress = 80
    private val path = Path()
    private var color = Color.RED

    private lateinit var mPaint: Paint
    private lateinit var mPaintProgress: Paint
    private lateinit var mRectF: RectF
    private lateinit var mTextPaint: Paint
    private var progressText = "$progress%"
    private val textBounds = Rect()

    private var centerY = 0

    private var centerX = 0

    private var swipeAngle = 0f

    init {
        context.withStyledAttributes(attrs, R.styleable.CustomProgressBar) {
            color = getColor(R.styleable.CustomProgressBar_progressColor, Color.RED)
            progress = getInt(R.styleable.CustomProgressBar_progress, 0)
        }

        initUI()
    }

    private fun initUI() {
        mPaint = Paint()
        mPaint.isAntiAlias = true
        mPaint.strokeWidth = 1f
        mPaint.style = Paint.Style.STROKE
        mPaint.color = color
        mPaintProgress = Paint()
        mPaintProgress.isAntiAlias = true
        mPaintProgress.style = Paint.Style.STROKE
        mPaintProgress.strokeWidth = 6f
        mPaintProgress.color = color
        mTextPaint = Paint()
        mTextPaint.isAntiAlias = true
        mTextPaint.style = Paint.Style.FILL
        mTextPaint.color = color
        mTextPaint.strokeWidth = 2f

        val percentage = progress * 100 / max
        swipeAngle = percentage * 360 / 100f
        progressText = "$percentage%"
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val viewWidth = MeasureSpec.getSize(widthMeasureSpec)
        val viewHeight = MeasureSpec.getSize(heightMeasureSpec)
        val radius: Int = (min(viewWidth, viewHeight) - 2) / 2
        path.reset()
        centerX = viewWidth / 2
        centerY = viewHeight / 2
        path.addCircle(centerX.toFloat(), centerY.toFloat(), radius.toFloat(), Path.Direction.CW)
        var smallCircleRadius: Int = radius - 4
        path.addCircle(
            centerX.toFloat(),
            centerY.toFloat(),
            smallCircleRadius.toFloat(),
            Path.Direction.CW
        )
        smallCircleRadius += 4
        mRectF = RectF(
            (centerX - smallCircleRadius).toFloat(),
            (centerY - smallCircleRadius).toFloat(),
            (centerX + smallCircleRadius).toFloat(),
            (centerY + smallCircleRadius).toFloat()
        )
        mTextPaint.textSize = radius * 0.5f
    }


    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawPath(path, mPaint)
        canvas.drawArc(mRectF, 270f, swipeAngle, false, mPaintProgress)
        drawTextCentred(canvas)
    }

    private fun drawTextCentred(canvas: Canvas) {
        mTextPaint.getTextBounds(progressText, 0, progressText.length, textBounds)
        canvas.drawText(
            progressText,
            centerX - textBounds.exactCenterX(),
            centerY - textBounds.exactCenterY(),
            mTextPaint
        )
    }

    fun setProgress(progress: Int) {
        this.progress = progress
        val percentage = progress * 100 / max
        swipeAngle = percentage * 360 / 100f
        progressText = "$percentage%"
        invalidate()
    }

    fun setColor(color: Int) {
        this.color = color
    }

    fun setMax(max: Int) {
        this.max = max
    }

}