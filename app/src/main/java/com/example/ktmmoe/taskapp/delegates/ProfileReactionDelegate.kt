package com.example.ktmmoe.taskapp.delegates

/**
 * Created by ktmmoe on 25, July, 2020
 **/
interface ProfileReactionDelegate {
    fun onShareClicked()
    fun onMessageClicked()
    fun onAssignTaskClicked()
}